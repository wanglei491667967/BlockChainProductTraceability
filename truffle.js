// Allows us to use ES6 in our migrations and tests.
/*var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonic = "turn execute engage antique current mention dial main elegant miracle crazy monkey";*/
require('babel-register')

module.exports = {
  networks: {
  development: {
    host: "192.168.3.142",
    port: 8545,
    network_id: 3 // match any network
  },
  live: {
    host: "https://mainnet.infura.io/232LYj2MB69CTPBS20At", // Random IP for example purposes (do not use)
    //port: 8545,
    network_id: "*",        // Ethereum public network
    // optional config values
    // gas
    // gasPrice
    // from - default address to use for any transaction Truffle makes during migrations
  }
}
}
/* networks: {
 ropsten: {
 provider: function() {
 return new HDWalletProvider(mnemonic,"https://mainnet.infura.io/232LYj2MB69CTPBS20At")
 },
 network_id: 3
 }
 }
};*/

