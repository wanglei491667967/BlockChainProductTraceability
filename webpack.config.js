const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './app/javascripts/app.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'app.js'
  },

  plugins: [
    // Copy our app's index.html to the build folder.
    new CopyWebpackPlugin([
      { from: './app/index.html', to: "index.html" },
       { from: './app/list-item.html', to: "list-item.html" },
       { from: './app/add.html', to: "add.html" },
        { from: './app/code.html', to: "code.html" },
         { from: './app/addinfo.html', to: "addinfo.html" },
          { from: './app/addinfo_arrival.html', to: "addinfo_arrival.html" },
           { from: './app/addinfo_departure.html', to: "addinfo_departure.html" },
            { from: './app/addinfo_factory.html', to: "addinfo_factory.html" },
            { from: './app/addinfo_storage.html', to: "addinfo_storage.html" },

    ])
  ],
  module: {
    rules: [
      {
       test: /\.css$/,
       use: [ 'style-loader', 'css-loader' ]
      }
    ],
    loaders: [
      { test: /\.json$/, use: 'json-loader' },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }
    ]
  },
  devServer:{
    host: '0.0.0.0',
    disableHostCheck:true,
    public:'0.0.0.0'  
  }
}
