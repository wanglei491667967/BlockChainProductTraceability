pragma solidity ^0.4.17;
pragma experimental ABIEncoderV2;

// This is just a simple example of a coin-like contract.
// It is not standards compatible and cannot be expected to talk to other
// coin/token contracts. If you want to create a standards-compliant
// token, see: https://github.com/ConsenSys/Tokens. Cheers!

contract MetaCoin {






	function MetaCoin() public {
        productIndex=0;
	
	}

    enum State { Picking, Packing, Dispatching, Fly, Fall, complete }

    struct Logistics {
        string owner;    // 中转站
        string date;    // 转运日期
        State status;   // 状态
        string message1; // 留言信息
        string message2; // 留言信息
         string message3; // 留言信息
          string message4; // 留言信息
    }

//商品的基本信息
uint public productIndex;
struct Product {
    //商品基本信息
    uint id;                    //商品编号，全局递增
    string name;                //商品名称
    string category;            //商品类别 (规格)
    string origin;              //产地
    string dateTime;            //区块生成时间
    address qklNumber;          //区块号

 }

struct Addressinfo {
    uint id;
    address Picking;
    address Packing;
    address Dispatching;
    address Fly;
    address Fall;
    address complete; 
}
mapping(string=>address[]) category;
mapping(uint => Product) stores;
//添加商品信息
function addProductToStore(string _name, string _category,string _origin,string _dateTime) public {
  productIndex += 1; //商品编号计数器递增
  Product memory product = Product(productIndex, _name, _category, _origin,_dateTime,0); //构造Product结构变量
  stores[productIndex] = product;//存入商品目录表 
}


    
    function getgoods(string _name, string _category,string _origin,string _dateTime) returns (bytes){
        this.call(_name,_category,_origin,_dateTime);
        return msg.data;
    }
    
     function getlogis(string _owner,string _date, State _status, string _message1,string _message2,string _message3,string _message4) returns (bytes){
        this.call(_owner,_date,_status,_message1,_message2,_message3,_message4);
        return msg.data;
    }    


 mapping (string => Logistics) stationss;
 function addLogistics(string _owner,string _date, State _status, string _message1,string _message2,string _message3,string _message4) public{
            Logistics memory node = Logistics(_owner,_date,_status,_message1,_message2,_message3,_message4);
        stationss[_owner]=node ;      
  }        
   
//获得中转信息
    function getLogisticss(string _owner) public view returns(string, string, State, string, string ,string,string) {
      Logistics memory node = stationss[_owner];
       
       return (node.owner,  node.date, node.status, node.message1,node.message2,node.message3,node.message4);
    }

  

}
